const {Router} = require('express')
const Link = require('../modal/Link')
const router = Router()

router.get('/:code', async (req, res) => {
    try {

        //получаем ссылку с кот. сейчас работаем
        const link = await Link.findOne({ code: req.params.code })

        if (link) {
            //подсчитываем кол-во клики и сохраняем
            link.clicks++
            await link.save()
            return res.redirect(link.from)
        }

        res.status(404).json('Ссылка не найдена')

    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
    }
})


module.exports = router
