const {Router} = require("express");
const Post = require("../modal/Post");
const router = Router()

router.get('/:id', async (req, res) => {

    try {
        const post = await Post.findById(req.params.id)
        res.json(post)
    } catch (e) {
        res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'})
    }

})


module.exports = router
