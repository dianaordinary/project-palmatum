# Project Palmatum
Фулстэк приложение.MERN.
Генерация(с сохранением) коротких ссылок

стек:

Бэк Часть(находится в корне project-palmatum):
Nodejs,express, mongoDB, mongoose - серверная часть (+ jwt авторизация)

app.js- сервер.


Фронтовая часть(client)
React , typescript 
Классическое SPA приложение.
для стилей использовалась либа materialize


## Структура client
основное фронтовое приложение находится в ```src```</br>

```.components``` - содержит кастомные компоненты</br>
```.context```  - контекст,содержащий какие-то базовые составляюшие</br>
```.hooks```  - хуки</br>
```.pages```  - страницы приложения</br>

```index.css```  - стили



## Как запустить
1. Создать БД mongoDB ( + настроить конфигурационные файлы в config )
2. Перейти в папку client. Выполнить команду ``npm i``
3. Запустить приложение из корня приложения project-palmatum ``npm run dev ``



![img.png](img.png)

Регистируемся и после заходим на сайт
![img_4.png](img_4.png)

Вводим длинную ссылку, жмем на энтер.
![img_5.png](img_5.png)

генерится короткий адрес (он сохраняется в БД и при вставки строчки будет происходить переход на изначальный адрес). Так же при 
клике по ссылке идет подсчет количество кликов.

![img_6.png](img_6.png)

Каталог сгенерированных ссылок( они уникальны, ссылки не дублируются в БД, произойдет редирект)

Также имеется раздел с постами, которые можно создавать, отсортировать, осуществлять по ним поиск.
![img_8.png](img_8.png)

![img_10.png](img_10.png)
![img_11.png](img_11.png)

## TODO List:

- в папке ```react-chess``` находится другое приложение(шахматы) чуть позже планирую подключить.
- раздел Аукцион тоже на стадии разработки.  
- добавить пагинацию в раздел Посты

