import React, {useEffect, useState} from 'react';
import "./App.css";
import BoardComponent from "./components/BoardComponent";
import {Board} from "./models/Board";
import {Player} from "./models/Player";
import {Colors} from "./models/Colors";
import ListFigures from "./components/ListFigures";
import Timer from "./components/Timer";
import ListMovesPlayersComponent from "./components/ListMovesPlayersComponent";

const App = () => {
    const [board, setBoard] = useState(new Board())


    const [whitePlayer, setWhitePlayer] = useState(new Player(Colors.WHITE))
    const [blackPlayer, setBlackPlayer] = useState(new Player(Colors.BLACK))
    const [currentPlayer, setCurrentPlayer] = useState<Player | null>(null);


    //хук ЮзЭффект
    useEffect(() => {
        restart()
        setCurrentPlayer(whitePlayer);
    }, [])

    function restart() {
        const newBoard = new Board();
        newBoard.init()
        newBoard.arrangeFigures()
        setBoard(newBoard)
    }

    function changePlayer() {
        setCurrentPlayer(currentPlayer?.color === Colors.WHITE ? blackPlayer : whitePlayer)
    }


    return (
        <div className="app">
            <Timer
                restart={restart}
                currentPlayer={currentPlayer}
            />
            <BoardComponent
                board={board}
                setBoard={setBoard}
                currentPlayer={currentPlayer}
                swapPlayer={changePlayer}
            />
            <div>
                <ListFigures
                    title="Белые фигуры"
                    figures={board.listBlackFigures}
                />
                <ListFigures
                    title="Черные фигуры"
                    figures={board.listWhiteFigures}
                />
            </div>
            <div>
                <ListMovesPlayersComponent
                    listMoves={board.listmoves}
                />
                <div>Правила</div>
                <div>Шах</div>
                <div>Мат</div>
            </div>
        </div>
    );
};

export default App;
