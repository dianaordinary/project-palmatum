import {Colors} from "./Colors";
import {Figure, FigureNames} from "./figures/Figure";
import {Board} from "./Board";
import {MovePlayer} from "./MovePlayer";


export class Cell {
     x: number;
     y: number;
    readonly color: Colors;
    available: boolean; // Можешь ли переместиться
    board: Board; // кольцевая зависимость (доска знает про доску, а доска про яцейки)
    figure: Figure | null;
    id: number; // Для реакт ключей

    constructor(board: Board, x: number, y: number, color: Colors, figure: Figure | null) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.figure = figure;
        this.board = board;
        this.available = false;
        this.id = Math.random()
    }

    isEmpty(): boolean {
        return this.figure === null;
    }

    moveFigure(potentialCell: Cell) {
        if (this.figure && this.figure?.canMove(potentialCell)) {
            this.figure.moveFigure(potentialCell)
            potentialCell.setFigure(this.figure);
            this.addListFigure(this.figure);
            this.figure = null;
        }
    }

    addListFigure(figure: Figure) {
        figure.color === Colors.BLACK
            ? this.board.listBlackFigures.push(figure)
            : this.board.listWhiteFigures.push(figure)
    }

    setFigure(figure: Figure) {
        this.figure = figure;
        this.figure.cell = this;
        this.addListMovesPlayers(this.figure, this.color, this.y, this.x);
    }


    //проверяется на пустату( по диагонали,вертикали и т.д.)
    isEmptyVertical(potentialCell: Cell): boolean {
        if (this.x !== potentialCell.x) {
            return false;
        }
        const min = Math.min(this.y, potentialCell.y);
        const max = Math.max(this.y, potentialCell.y);
        for (let y = min + 1; y < max; y++) {
            if (!this.board.getCell(this.x, y).isEmpty()) {
                return false;
            }
        }
        return true;
    }

    isEmptyHorizontal(potentialCell: Cell): boolean {
        if (this.y !== potentialCell.y) {
            return false;
        }
        const min = Math.min(this.x, potentialCell.x);
        const max = Math.max(this.x, potentialCell.x);
        for (let x = min + 1; x < max; x++) {
            if (!this.board.getCell(x, this.y).isEmpty()) {
                return false
            }
        }
        return true;
    }

    isEmptyDiagonal(potentialCell: Cell): boolean {
        const absX = Math.abs(potentialCell.x - this.x);
        const absY = Math.abs(potentialCell.y - this.y);
        if (absY !== absX) {
            return false
        }
        const dy = this.y < potentialCell.y ? 1 : -1;
        const dx = this.x < potentialCell.x ? 1 : -1;

        for (let i = 1; i < absY; i++) {
            if (!this.board.getCell(this.x + dx * i, this.y + dy * i).isEmpty()) {
                return false;
            }
        }
        return true;
    }

    isEnemy(potentialCell: Cell): boolean {
        if (potentialCell.figure) {
            return this.figure?.color !== potentialCell.figure.color;
        }
        return false;
    }


    //для записи ходов
    addListMovesPlayers(figure: Figure, color: Colors, y: number, x: number) {
        let move: MovePlayer = {color, figure, y, x}
        this.board.listmoves.push(move)
    }


}
