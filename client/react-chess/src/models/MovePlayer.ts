import {Figure} from "./figures/Figure";
import {Colors} from "./Colors";
import {Player} from "./Player";

export class MovePlayer {
    readonly x: number;
    readonly y: number;

    color: Colors;
    figure: Figure;

    constructor(color: Colors, figure: Figure, x: number, y: number) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.figure = figure;
    }
}
