import {Cell} from "./Cell";
import {Colors} from "./Colors";
import {Pawn} from "./figures/Pawn";
import {King} from "./figures/King";
import {Queen} from "./figures/Queen";
import {Bishop} from "./figures/Bishop";
import {Knight} from "./figures/Knight";
import {Rook} from "./figures/Rook";
import {Figure, FigureNames} from "./figures/Figure";
import {MovePlayer} from "./MovePlayer";
import {CheckmateDetector} from "./CheckmateDetector";

var _ = require('lodash');

export class Board {
    cells: Cell[][] = [];
    listBlackFigures: Figure[] = []
    listWhiteFigures: Figure[] = []
    listmoves: MovePlayer[] = [];
   // cmd:CheckmateDetector;

    // Кольцевая зависимость
    // Кольцевая зависимость
    public init() {
        for (let i = 0; i < 8; i++) {
            const row: Cell[] = []
            for (let j = 0; j < 8; j++) {
                if ((i + j) % 2 !== 0) {
                    row.push(new Cell(this, j, i, Colors.BLACK, null)) // Черные ячейки
                } else {
                    row.push(new Cell(this, j, i, Colors.WHITE, null)) // белые
                }
            }
            this.cells.push(row);
        }
       //this.cmd = CheckmateDetector(this, Wpieces, Bpieces, wk, bk);
    }

    public getCell(x: number, y: number) {
        return this.cells[y][x]
    }

    private addPawns() {
        for (let i = 0; i < 8; i++) {
            new Pawn(Colors.BLACK, this.getCell(i, 1))
            new Pawn(Colors.WHITE, this.getCell(i, 6))
        }
    }

    private addKings() {
        new King(Colors.BLACK, this.getCell(4, 0));
        new King(Colors.WHITE, this.getCell(4, 7))
    }

    private addQueens() {
        new Queen(Colors.BLACK, this.getCell(3, 0))
        new Queen(Colors.WHITE, this.getCell(3, 7))
    }

    private addBishops() {
        new Bishop(Colors.BLACK, this.getCell(2, 0))
        new Bishop(Colors.BLACK, this.getCell(5, 0))
        new Bishop(Colors.WHITE, this.getCell(2, 7))
        new Bishop(Colors.WHITE, this.getCell(5, 7))
    }

    private addKnights() {
        new Knight(Colors.BLACK, this.getCell(1, 0))
        new Knight(Colors.BLACK, this.getCell(6, 0))
        new Knight(Colors.WHITE, this.getCell(1, 7))
        new Knight(Colors.WHITE, this.getCell(6, 7))
    }

    private addRooks() {
        new Rook(Colors.BLACK, this.getCell(0, 0))
        new Rook(Colors.BLACK, this.getCell(7, 0))
        new Rook(Colors.WHITE, this.getCell(0, 7))
        new Rook(Colors.WHITE, this.getCell(7, 7))
    }

    public highlightCells(selectedCell: Cell | null) {
        let list: any[] = [];
        for (let i = 0; i < this.cells.length; i++) {
            const row = this.cells[i];
            for (let j = 0; j < row.length; j++) {
                const potentialCell = row[j];
                potentialCell.available = !!selectedCell?.figure?.canMove(potentialCell) //todo
                if ( potentialCell.available) {
                    let clone = _.cloneDeep(potentialCell);
                    list.push(clone)
                }
            }
        }
        for (let i = 0; i < list.length; i++) {
            if (list[i].figure?.name === FigureNames.KING) {
                console.log("list[i].figure")
                console.log(list[i].figure)
            }
        }
    }

    getCopyBoard(): Board {
        const newBoard = new Board();
        newBoard.cells = this.cells  // ПЕРЕНОСИМ текущие объекты в новую доску

        newBoard.listWhiteFigures = this.listWhiteFigures
        newBoard.listBlackFigures = this.listBlackFigures

        newBoard.listmoves = this.listmoves;
        return newBoard;
    }

    public arrangeFigures() {
        this.addPawns()
        this.addKnights()
        this.addKings()
        this.addBishops()
        this.addQueens()
        this.addRooks()
    }
}
