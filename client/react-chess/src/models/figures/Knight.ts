import {Figure, FigureNames} from "./Figure";
import {Colors} from "../Colors";
import {Cell} from "../Cell";
import blackLogo from "../../assets/black-knight.png";
import whiteLogo from "../../assets/white-knight.png";

export class Knight extends Figure {
    constructor(color: Colors, cell: Cell) {
        super(color, cell);
        this.logo = color === Colors.BLACK ? blackLogo : whiteLogo;
        this.name = FigureNames.KNIGHT;
    }

    canMove(potentialCell: Cell): boolean {
        if (!super.canMove(potentialCell))
            return false;
        //разница - дельта
        const dx = Math.abs(this.cell.x - potentialCell.x);
        const dy = Math.abs(this.cell.y - potentialCell.y);

        return (dx === 1 && dy === 2) || (dx === 2 && dy === 1)
    }
}
