import {Figure, FigureNames} from "./Figure";
import {Colors} from "../Colors";
import {Cell} from "../Cell";
import blackLogo from "../../assets/black-queen.png";
import whiteLogo from "../../assets/white-queen.png";

export class Queen extends Figure {
    constructor(color: Colors, cell: Cell) {
        super(color, cell);
        this.logo = color === Colors.BLACK ? blackLogo : whiteLogo;
        this.name = FigureNames.QUEEN;
    }

    canMove(potentialCell: Cell): boolean {
        if(!super.canMove(potentialCell))
            return false;
        if(this.cell.isEmptyVertical(potentialCell))
            return true;
        if(this.cell.isEmptyHorizontal(potentialCell))
            return true;
        if(this.cell.isEmptyDiagonal(potentialCell))
            return true;
        return false
    }
}
