import {Figure, FigureNames} from "./Figure";
import {Cell} from "../Cell";
import {Colors} from "../Colors";
import blackLogo from '../../assets/black-bishop.png'
import whiteLogo from '../../assets/white-bishop.png'


export class Bishop extends Figure {

    constructor(color: Colors, cell: Cell) {
        super(color, cell);
        this.logo = color === Colors.BLACK ? blackLogo : whiteLogo;
        this.name = FigureNames.BISHOP;
    }

    canMove(potentialCell: Cell): boolean {
        if (!super.canMove(potentialCell)) {
            // если этот метод возвращает фолс,то тоже возв. фолс
            return false;
        }
        if (this.cell.isEmptyDiagonal(potentialCell))
            return true
        return false
    }
}
