import {Figure, FigureNames} from "./Figure";
import {Colors} from "../Colors";
import {Cell} from "../Cell";
import blackLogo from "../../assets/black-pawn.png";
import whiteLogo from "../../assets/white-pawn.png";

export class Pawn extends Figure {

    isFirstStep: boolean = true;

    constructor(color: Colors, cell: Cell) {
        super(color, cell);
        this.logo = color === Colors.BLACK ? blackLogo : whiteLogo;
        this.name = FigureNames.PAWN;
    }

    canMove(potentialCell: Cell): boolean {
        if(!super.canMove(potentialCell))
            return false;
        const direction = this.cell.figure?.color === Colors.BLACK ? 1 : -1
        const firstStepDirection = this.cell.figure?.color === Colors.BLACK ? 2 : -2

        if ((potentialCell.y === this.cell.y + direction || this.isFirstStep
                && (potentialCell.y === this.cell.y + firstStepDirection))
            && potentialCell.x === this.cell.x
            && this.cell.board.getCell(potentialCell.x, potentialCell.y).isEmpty()) {
            return true;
        }

        if(potentialCell.y === this.cell.y + direction
            && (potentialCell.x === this.cell.x + 1 || potentialCell.x === this.cell.x - 1)
            && this.cell.isEnemy(potentialCell)) {
            return true;
        }
        return false;
    }

    moveFigure(potentialCell: Cell) {
        super.moveFigure(potentialCell);
        this.isFirstStep = false;
    }

    testVoid(figure:Figure) {
        debugger
        figure.name=FigureNames.KNIGHT;
    }
}
