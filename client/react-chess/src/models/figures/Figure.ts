import logo from '../../assets/black-king.png'
import {Colors} from "../Colors";
import {Cell} from "../Cell";


export enum FigureNames {
    FIGURE = "Фигура",
    KING = "Король",
    KNIGHT = "Конь",
    PAWN = "Пешка",
    QUEEN = "Ферзь",
    ROOK = "Ладья",
    BISHOP = "Слон",
}

export class Figure {
    color: Colors;
    logo: typeof logo | null;
    cell: Cell; // опять же кольцевая зависимость
    name: FigureNames;
    id: number;

    kingAttacked: any;

    constructor(color: Colors, cell: Cell) {
        this.color = color;
        this.cell = cell;
        this.cell.figure = this; // ХАК. Поэтому удобна кольцевая зависимость
        this.logo = null;
        this.name = FigureNames.FIGURE //кольцевая зависимость

        this.id = Math.random()
    }

    // potentialCell - ячейка на кот хотим переместиться
    // наверное самый главный метод.
    // логика перемещения
    canMove(potentialCell: Cell): boolean {
        //console.log(potentialCell)
        //свои фигуры есть нельзя
        if (potentialCell.figure?.color === this.color)
            return false
        //короля съесть не можем.но его нужно проверять на шах и мат
        if (potentialCell.figure?.name === FigureNames.KING) {
            // this.testVoid(potentialCell.figure);
            // координаты противоположенного короля let x = potentialCell.x; let y = potentialCell.y;
            return false
        }
        return true;

    }

    moveFigure(potentialCell: Cell) {
    }

    //удалить целевые позиции, где король будет в беде после хода


    isUnderAttack(cell: Cell) {
        return null;
    }

    kingUnderAttack(figure: Figure) {
    }

    testVoid(figure: Figure) {
        alert(" TEST " + figure.name)
    }

}
