import React, {FC, useEffect, useRef, useState} from 'react';
import {Player} from "../models/Player";
import {Figure} from "../models/figures/Figure";
import {MovePlayer} from "../models/MovePlayer";


interface MoveProps {
    listMoves: MovePlayer[]
}

function convert(x:number){
    let output = '';
    switch (x) {
        case 0:
            output = 'a';
            break
        case 1:
            output = 'b';
            break
        case 2:
            output = 'c';
            break
        case 3:
            output = 'd';
            break
        case 4:
            output = 'e';
            break
        case 5:
            output = 'f';
            break
        case 6:
            output = 'g';
            break
        case 7:
            output = 'h';
            break
    }
 return output;
}

function revert(y:number){
    return (8-y);
}

const Moves: FC<MoveProps> = ({listMoves}) => {
    return (
        <div className="listfigures">
            <h3>Ходы:</h3>
            {listMoves.map(moves =>
                <div>
                    {moves.figure.name} {moves.figure.logo && <img width={20} height={20} src={moves.figure.logo}/>}
                    {convert(moves.x)} {revert(moves.y)}
                </div>
            )}
        </div>
    );
};


export default Moves;
