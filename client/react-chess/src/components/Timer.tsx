import React, {FC, useEffect, useRef, useState} from 'react';
import {Player} from "../models/Player";


interface TimerProps {
  currentPlayer: Player | null;
  restart: () => void;
}

const Timer: FC<TimerProps> = ({currentPlayer, restart}) => {
  return (
      <div className="time"></div>
  );
};

export default Timer;
