import React from 'react'

import {LinksPage} from "./pages/LinksPage";
import {DetailPage} from './pages/DetailPage'
import {AuthPage} from './pages/AuthPage'
import {CreatePage} from './pages/CreatePage'

import {Route} from 'react-router';
import {Routes, Navigate, Redirect} from "react-router-dom";
import {PostsPage} from "./pages/PostsPage";
import {TemplatePage} from "./pages/TemplatePage";
import {AuctionPage} from "./pages/AuctionPage";
import {PostPage} from "./pages/PostPage";

//определили набор роутов
export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Route path="/links" exact element={<LinksPage/>}></Route>
                <Route path="/create" exact element={<CreatePage/>}></Route>
                <Route path="/detail/:id" element={<DetailPage/>}></Route>
                <Route path="/template" exact element={<TemplatePage/>}></Route>
                <Route path="/posts" exact element={<PostsPage/>}></Route>

                <Route path="/posts/:id" exact element={<PostPage/>}></Route>

                <Route path="/auction" exact element={<AuctionPage/>}></Route>
            </Routes>
        )
    }
    //чел не прошел авторизацию
    return (
        <Routes>
            <Route path="/" exact element={<AuthPage/>}> </Route>
        </Routes>
    )
}

//import {Routes, Navigate} from "react-router-dom";
/*
export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Navigate path="/links" exact>
                    <LinksPage/>
                </Navigate>
                <Navigate path="/create" exact>
                    <CreatePage/>
                </Navigate>
                <Navigate path="/detail/:id">
                    <DetailPage/>
                </Navigate>
                <Navigate to="/create"/>
            </Routes>
        )
    }

    return (
        <Routes>
            <Navigate path="/" exact>
                <AuthPage/>
            </Navigate>
            <Navigate to="/"/>
        </Routes>
    )

}
*/
