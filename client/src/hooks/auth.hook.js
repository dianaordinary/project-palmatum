import {useState, useCallback, useEffect} from 'react'

const storageName = 'userData'

//модуль кот работает только с авторизацией юзером
export const useAuth = () => {
    //локальный стейт
    const [token, setToken] = useState(null)
    const [ready, setReady] = useState(false)
    const [userId, setUserId] = useState(null)

    const login = useCallback((jwtToken, id) => {
        setToken(jwtToken)
        setUserId(id)
        //базовый браузерный api
        localStorage.setItem(storageName, JSON.stringify({
            userId: id, token: jwtToken
        }))
    }, [])


    const logout = useCallback(() => {
        setToken(null)
        setUserId(null)
        localStorage.removeItem(storageName)
    }, [])


    // если есть в localStorage, то используем эти потенциальные данные
    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName))

        if (data && data.token) {
            login(data.token, data.userId)
        }
        setReady(true)// модуль авторизации отработал
    }, [login])


    //экспортируем login, logout, token, userId, ready
    return { login, logout, token, userId, ready }
}
