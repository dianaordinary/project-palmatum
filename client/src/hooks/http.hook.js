import {useState, useCallback} from "react";

export const useHttp = () => {
    // хук для взаимодействия с сервером.
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    //что происходит когда делаем реквест
    const request = useCallback(async (url, method = "GET", body = null, headers = {}) => {
         setLoading(true);
        try {

            if (body) {
                body = JSON.stringify(body)
                headers['Content-Type'] = 'application/json'
            }
            const response = await fetch(url, {method, body, headers})
            const data = await response.json();

            //если с запросом что-то не так
            if (!response.ok) {
                throw new Error(data.message || 'Что-то пошло не так')
            }
            setLoading(false);
            return data;
        } catch (e) {
            //есть ошибка
            setLoading(false);
            setError(e.message)
            throw e; //выкидываем ошибку!
        }
    }, [])

    const clearError = useCallback(() => setError(null), [])

    return {loading, request, error, clearError}
}
