import {useCallback} from 'react'

export const useMessage = () => {
    return useCallback(text => {
        // объект из материалайза
        if (window.M && text) {
            window.M.toast({ html: text })
        }
    }, [])
}
