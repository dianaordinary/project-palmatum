import {useState} from "react";

// обработка индекатора загрузки
export const useFetching = (callback) => {

    // состояние, которое отвечает за загрузку
    const [isLoading, setIsLoading] = useState(false);
    // базовый кейс, обработки ошибки
    const [error, setError] = useState('');

    const fetching = async (...args) => {
        try {
            setIsLoading(true)
            await callback(...args)
        } catch (e) {
            setError(e.message);
        } finally {
            setIsLoading(false)
        }
    }

    return [fetching, isLoading, error]
}
