//пользовательский хук сортировки постов
import {useMemo} from "react";

export const useSortedPosts = (posts, sortAlgorithm) => {
    const sortedPosts= useMemo(()=>{
        if(sortAlgorithm) {
            return [...posts].sort((a, b) => a[sortAlgorithm].localeCompare(b[sortAlgorithm]));
        }
        return posts;
    },[sortAlgorithm,posts])
     return sortedPosts;
}


//пользовательский хук получения отсортированных и отфильтрованных постов
export const usePosts = (posts, sortAlgorithm, queryParam) => {
    //получаем отсортированные посты
    const sortedPosts = useSortedPosts(posts,sortAlgorithm);
    const sortedAndSearchedPosts = useMemo( ()=>{
        return sortedPosts.filter( post => post.title.toLowerCase().includes(queryParam.toLowerCase()))
    },[queryParam,sortedPosts])
    return sortedAndSearchedPosts;
}
