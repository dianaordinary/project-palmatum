import React from 'react';
import classes from './PalmInput.module.css';

const PalmInput = React.forwardRef((props, ref) => {
    return (
        <input ref={ref} className={classes.myInput} {...props}/>
    );
});

export default PalmInput;
