import React, {useRef, useState} from 'react';
import './PalmWheel.css';
import PalmButton from "../button/PalmButton";


const PalmWheel = ({lots, onFinished}) => {

        console.log("lots", lots)

        const [isFinished, setFinished] = useState(false)

        const [state, setState] = useState({
            name: 'spinner'
        })

        let randomTime = Math.floor(Math.random() * 10000) + 1;
        let currentSegment = ''

        const startRotation = () => {
            setState({
                name: "spinner rotating"
            })
            setTimeout(() => {
                setState({
                    name: "spinner rotating paused_rotating"
                })
                onFinished(currentSegment)
            }, randomTime)
        }

        return (
            <div className="spinner_container">
                <div className="arrow"></div>
                <ul className={state.name}>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>1
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>2
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>3
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>4
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>5
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>6
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>7
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>8
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>9
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>10
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>11
                        </div>
                    </li>
                    <li className="spinner_li">
                        <div className="spinner_text"
                             suppressContentEditableWarning={true}
                             spellCheck={false}>12
                        </div>
                    </li>
                </ul>
                <div className="spin_button">
                    <PalmButton onClick={startRotation}>GO!</PalmButton>
                </div>
            </div>
        );
    }
;

export default PalmWheel;
