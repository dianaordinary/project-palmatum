import React from 'react';
import cl from './PalmLoader.module.css';

const PalmLoader = ({isLinerLoader}) => {
    if (isLinerLoader) {
        return (
            <div>
                <div className="progress">
                    <div className="indeterminate"></div>
                </div>
            </div>)
    }
    return (<div>
            <div className="preloader-wrapper active">
                <div className="spinner-layer spinner-red-only">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div>
                    <div className="gap-patch">
                        <div className="circle"></div>
                    </div>
                    <div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default PalmLoader;
