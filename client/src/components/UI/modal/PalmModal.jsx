import React from 'react';
import cl from './PalmModal.module.css';

const PalmModal = ({children, visible, setVisible,modalTitle}) => {

    const rootClasses = [cl.myModal]

    if (visible) {
        rootClasses.push(cl.active);
    }

    return (
        <div className={rootClasses.join(' ')}
             onClick={() => setVisible(false)}>
            <div className={cl.myModalContent}
                 onClick={(e) => {
                     //отменяем всплытие события
                     e.stopPropagation()}}>
                <h3>{modalTitle}</h3>
                {children}
            </div>
        </div>
    );
};

export default PalmModal;
