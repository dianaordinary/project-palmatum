import React from 'react';
import './PalmButton.css';

const PalmButton = ({children,...props}) => {
    //класс как свойство объекта
    return (
/*        <button {...props}  className={classes.myBtn}>
            {children}
        </button>*/
        <button {...props}  className="btn teal darken-3" style={{margin: 5}}>
            {children}
        </button>
    );
};

export default PalmButton;
