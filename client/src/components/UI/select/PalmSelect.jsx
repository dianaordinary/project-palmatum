import React from 'react';
import classes from './PalmSelect.module.css';

const PalmSelect = ({options, defaultValue, value, onChange}) => {

    // реализовано двусторонее связывание. чтобы сделать компонент управляемым (value, onChange)
    return (
        <select
            value={value}
            onChange={event => onChange(event.target.value)}
            className={classes.mySelect}
        >
            <option disabled value="">{defaultValue}</option>
            {options.map(option =>
                <option key={option.value} value={option.value}>
                    {option.name}
                </option>
            )}
        </select>
    );
};

export default PalmSelect;
