import React from 'react';
import PalmInput from "../UI/input/PalmInput";
import PalmSelect from "../UI/select/PalmSelect";

const PostFilter = ({filter,setFilter}) => {
    // два состояния:
    // 1. для сортировки
    // 2. для поиска строки


    return (
        <div style={{padding: 10}}>
            <PalmInput
                value={filter.queryParam}
                onChange={e=>setFilter({...filter,queryParam:e.target.value})}
                placeholder="Поиск..."
            />
            <PalmSelect
                defaultValue="Сортировка"
                options={[
                    {value: 'title', name: 'По названию'},
                    {value: 'body', name: 'По описанию'},
                ]}
                value={filter.sortAlgorithm}
                onChange={selectedSort => setFilter({...filter,sortAlgorithm:selectedSort})}
            />
        </div>
    );
};

export default PostFilter;
