import React, {useRef, useState} from 'react';
import PalmInput from "../UI/input/PalmInput";
import PalmButton from "../UI/button/PalmButton";
import PostList from "./PostList";
import PalmSelect from "../UI/select/PalmSelect";

const PostForm = ({create}) => {


    const [post, setPost] = useState({title:'',body:''});

    const [title, setTitle] = useState('');
    const [body, setBody] = useState(''); // управляемый
    const refInput = useRef(); // неуправляемый

    const addNewPost = (e) => {
        e.preventDefault(); // удаление работы браузера по умолчанию
        const newPost = {
            ...post, id: Date.now()
        }
        create(newPost);
        setPost({title: '',body: ''}); //отчищаем инпут
    }



    return (
        <div>
            {/*<PalmInput
                type="text"
                placeholder="Заголовок"
                value={title}
                onChange={e => setTitle(e.target.value)}/>
            <PalmInput type="text"
                       placeholder="Описание"
                       ref={refInput}/>
            <PalmInput
                type="text"
                placeholder="Описание"
                value={body}
                onChange={e => setBody(e.target.value)}/>*/}
            <PalmInput
                type="text"
                placeholder="Заголовок"
                value={post.title}
                onChange={e => setPost({...post, title: e.target.value})}/>
            <PalmInput
                type="text"
                placeholder="Описание"
                value={post.body}
                onChange={e => setPost({...post, body:e.target.value})}/>
            <PalmButton onClick={addNewPost}>Добавить</PalmButton>

        </div>
    );
};

export default PostForm;
