import {useState} from "react";

export const Counter = () => {
    /*let likes = 0;
    function increment(){likes +=1;}
    function decrement(){likes -=1;}*/
    // функийя setLikesState, которая меняет состояние likesState
    const [likesState, setLikesState] = useState(0);

    function increment() {
        setLikesState(likesState + 1);
    }
    function decrement() {
        setLikesState(likesState - 1);
    }


    return (
        <div className="counter">
                <button onClick={increment}>+</button>
                <button onClick={decrement}>-</button>

            Колличество лайков: {likesState}
        </div>
    )
}
