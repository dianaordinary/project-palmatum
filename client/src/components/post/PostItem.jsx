import React from 'react';
import {useNavigate} from 'react-router-dom';
import PalmButton from "../UI/button/PalmButton";


const PostItem = (props) => {

    const navigate = useNavigate();

    return (
        <div className="post">

            <div className="post__content">
                <strong>{props.number}. {props.post.title}</strong>
                <div>
                    {props.post.body}
                </div>
            </div>

            <div className="post__btns">
                <PalmButton onClick={() => navigate(`/posts/${props.post.id}`)}>
                    Открыть
                </PalmButton>
                <PalmButton onClick={() => props.remove(props.post)}>
                    Удалить
                </PalmButton>
            </div>
        </div>
    );
};

export default PostItem;
