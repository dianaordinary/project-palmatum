import React, {useContext} from 'react'
import {NavLink, useNavigate } from 'react-router-dom'
import {AuthContext} from '../context/AuthContext'

export const Navbar = () => {
    const navigate  = useNavigate(); //todo
    const auth = useContext(AuthContext)

    const logoutHandler = event => {
        event.preventDefault()
        auth.logout()
        navigate("/"); // редирект
    }

    return (
        <nav>
            <div className="nav-wrapper red lighten-1" style={{padding: '0 2rem'}}>
                <span className="brand-logo">palmatum</span>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><NavLink to="/create">Сгенерировать</NavLink></li>
                    <li><NavLink to="/links">Ссылки</NavLink></li>
                    <li><NavLink to="/auction">Аукцион</NavLink></li>
                    <li><NavLink to="/posts">Посты</NavLink></li>
                    <li><NavLink to="/template">Шаблонизатор</NavLink></li>
                    <li><a href="/" onClick={logoutHandler}>Выйти</a></li>
                </ul>
            </div>
        </nav>
    )
}
