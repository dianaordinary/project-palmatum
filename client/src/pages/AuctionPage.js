import React, {useState} from "react";
import PalmInput from "../components/UI/input/PalmInput";
import PalmButton from "../components/UI/button/PalmButton";
import PalmWheel from "../components/UI/wheel/PalmWheel";



export const AuctionPage = () => {


    const addNewLot = (e) => {
        e.preventDefault(); // удаление работы браузера по умолчанию

    }

    const [points, setPoints] = useState(false);

    let lots = [
        {
            number: 1,
            text: "Скидка 10%",
            color: "hsl(197 30% 43%)",
        },
        {
            text: "Дизайн в подарок",
            color: "hsl(173 58% 39%)",
        },
        {
            number: 2,
            text: "Второй сайт бесплатно",
            color: "hsl(43 74% 66%)",
        },
        {
            number: 3,
            text: "Скидка 50%",
            color: "hsl(27 87% 67%)",
        },
        {
            number: 4,
            text: "Блог в подарок",
            color: "hsl(12 76% 61%)",
        },
        {
            number: 5,
            text: "Скидок нет",
            color: "hsl(350 60% 52%)",
        },
        {
            number: 6,
            text: "Таргет в подарок",
            color: "hsl(91 43% 54%)",
        },
        {
            number: 7,
            text: "Скидка 30% на всё",
            color: "hsl(140 36% 74%)",
        }]

    const onFinished = (winner) => {
        alert(winner);
    };

    return (
        <div className="hg container">

            <div className="hg-header">
                <h1>Auction page</h1>

                <div>
                    Вариант:
                    <PalmInput
                        type="text"
                        placeholder="Вариант"
                    />
                    сумма:
                    <PalmInput
                        type="text"
                        placeholder="сумма"
                    />
                </div>

              {/*  <PalmButton onClick={addNewOption}>Добавить</PalmButton>*/}
            </div>

            <div className="hg-menu">
                <p>
                    Таблица вариантов:
                </p>
                <table>
                    <thead>
                    <tr>
                        <th>Позиция</th>
                        <th>Вариант</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Alvin</td>
                        <td>$0.87</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div className="hg-main">
                <PalmWheel
                    lots={lots}
                    onFinished={(winner) => onFinished(winner)}
                />
            </div>

            <div className="hg-footer">
                Выиграл вариант: Footer
            </div>

        </div>
    )
}
