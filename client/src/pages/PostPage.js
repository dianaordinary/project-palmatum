import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {useFetching} from "../hooks/useFetching";
import PostService from "../API/PostService";
import {Loader} from "../components/Loader";

export const PostPage = () => {

    //  айди из ввода в адрессной строки(исп.ем хук)
    const paramId= useParams().id;
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);

    // todo переделать на более правильный запрос! как в DetailPage
    const [fetchPostById, isLoading, error] = useFetching(async (id) => {
        const response = await PostService.getByIdAxios(id)
        setPost(response.data);
    })
    const [fetchComments, isComLoading, comError] = useFetching(async (id) => {
        const response = await PostService.getCommentsByPostIdAxios(id)
        setComments(response.data);
    })

    useEffect(() => {
        fetchPostById(paramId)
        fetchComments(paramId)
    }, [])



    return (
        <div>
            <h1>Post page</h1>
            {isLoading
                ? <Loader/>
                :
            <div>
                <h3>{post.title}</h3>
                <div>{post.body}</div>
            </div>
            }

            <h3>
                комментарии
            </h3>
            {isComLoading
                ? <Loader/>
                : <div>
                    {comments.map(comm =>
                        <div key={comm.id} className="comment">
                            <div>{comm.body}</div>
                            <h6>email: {comm.email}</h6>
                        </div>
                    )}
                </div>
            }

        </div>
    )
}
