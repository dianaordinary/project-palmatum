//import React from "@types/react";

import React, {useEffect, useMemo, useRef, useState} from "react";
import PostList from "../components/post/PostList";
import PalmInput from "../components/UI/input/PalmInput";
import PalmButton from "../components/UI/button/PalmButton";
import PostForm from "../components/post/PostForm";
import PalmSelect from "../components/UI/select/PalmSelect";
import PostFilter from "../components/post/PostFilter";
import PalmModal from "../components/UI/modal/PalmModal";
import {usePosts} from "../hooks/usePosts";
import axios from "axios";
import PostService from "../API/PostService";
import PalmLoader from "../components/UI/loader/PalmLoader";
import {Loader} from "../components/Loader";
import {useFetching} from "../hooks/useFetching";

export const PostsPage = () => {

    const [posts, setPostsState] = useState([])
    //const [isLoading, setIsLoading] = useState(false)

    const [filter, setFilter] = useState({sortAlgorithm: '', queryParam: ''});
    // const  [selectedSort, setSelectedSort] =useState('');
    // const [searchQuery, setSearchQuery] = useState('');
    const [modal, setModal] = useState(false);

    //кастомный хук
    const sortedAndSearchedPosts = usePosts(posts, filter.sortAlgorithm, filter.queryParam)

    // функция обратного вызова
    const createPost = (newPost) => {
        setPostsState([...posts, newPost]);
        setModal(false);
    }

    // Получаем post из дочернего компонента
    const removePost = (post) => {
        setPostsState(posts.filter(p => p.id !== post.id));
    }

    // кастомный хук, обработка индикации загрузки на получение данных
    // хук вернет массив fetchPosts, isLoading, postError
    const [fetchPosts, isLoading, postError] = useFetching(async ()=>{
        const response = await PostService.getPostsAxios();
        setPostsState(response.data);
    })

    useEffect(() => {
        fetchPosts();
    }, [])


    const [inputState, setInputState] = useState('Текст');

    //двухстороннее связывание. Управлякмая компонента
    function enter(event) {
        setInputState(event.target.value);
    }

    //двухстороннее связывание. Управлякмая компонента
    // колбэк! Не функция
    // Pass event as a parameter in the method this is an example:
    const onChangeHandaler = (event) => {
        setInputState(event.target.value);
    }
    /*<Counter/>
    <input type="text"
           value={inputState}
           onChange={onChangeHandaler}/>
    Ввели: {inputState}*/

    return (
        <div>
            <h1>Posts page</h1>
            {/* <PalmButton onClick={ fetchPosts }>qqq</PalmButton>*/}
            <PalmButton onClick={() => setModal(true)}>Создать пост</PalmButton>
            <PalmModal visible={modal}
                       setVisible={setModal}
                       modalTitle="Пост">
                <PostForm create={createPost}/>
            </PalmModal>
            <PostFilter filter={filter}
                        setFilter={setFilter}/>
            {isLoading ?
                <PalmLoader isLinerLoader={true}/>
                /*<Loader/>*/
            :
                <PostList title="Список постов"
                          remove={removePost}
                          posts={sortedAndSearchedPosts}/>
            }
            <div className="footer" style={{paddingBottom: 50}}/>
        </div>
    )
}
