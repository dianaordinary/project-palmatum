import {createContext} from 'react'

//функция,кот ничего не делает
function fn() {}

export const AuthContext = createContext({
    token: null,
    userId: null,
    login: fn,
    logout: fn,
    isAuthenticated: false
})
