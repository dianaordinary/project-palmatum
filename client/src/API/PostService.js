import axios from "axios";

export default class PostService {

    static async getPostsAxios(){
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
        return response;
    }

    static async fetchPosts() {
        const res = await fetch("https://jsonplaceholder.typicode.com/posts");
        let data = await res.json();
        return data;
       /* const res = await fetch("https://jsonplaceholder.typicode.com/posts")
            .then(res => res.json())
            .then((result) => {
                    return result;
                },
                (error) => {
                    console.log('что-то не так')
                }
            )*/
    }



    static async getAllAxios(limit = 10, page = 1) {
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts', {
            params: {
                _limit: limit,
                _page: page
            }
        })
        return response;
    }

    static async getByIdAxios(id) {
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
        return response;
    }

    static async getCommentsByPostIdAxios(id) {
        const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
        return response;
    }
}
