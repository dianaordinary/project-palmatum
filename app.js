const express = require('express')
const config = require('config')
const path = require('path')
const mongoose = require('mongoose')

const serverApp = express()
//регестрация
serverApp.use(express.json({ extended: true }))  //todo встроенный мидлвээр!

//подключаем роуты
serverApp.use('/api/auth', require('./routes/auth.routes'))
serverApp.use('/api/link', require('./routes/link.routes'))
serverApp.use('/t', require('./routes/redirect.routes'))

serverApp.use('/api/post', require('./routes/post.routes'))
serverApp.use('/api/auction', require('./routes/post.routes'))

if (process.env.NODE_ENV === 'production') {
    serverApp.use('/', express.static(path.join(__dirname, 'client', 'build')))

    serverApp.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const PORT = config.get('port') || 5000

async function start() {
    try {
        await mongoose.connect(config.get('mongoUri'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        serverApp.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}
start();





