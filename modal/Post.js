const {Schema, model, Types} = require('mongoose')

//особый тип в монгоДБ - Types.ObjectId. СВЯЗКА в бд
const schema = new Schema({
    userId: {type: String},
    id: {type: String, required: true, unique: true},
    title: {type: String},
    body: {type: String},
})

module.exports = model('Post', schema)
